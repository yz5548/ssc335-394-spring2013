Midpoint
k = 10, error = -0.00289113
k = 100, error = 0.0103255
k = 1000, error = 0.000995487
k = 10000, error = 3.317e-05
Trapezoid
k = 10, error = -0.0681106
k = 100, error = 5.77867e-05
k = 1000, error = -7.10785e-06
k = 10000, error = -6.68466e-05
Simpson
k = 10, error = -0.0875491
k = 100, error = -0.00482898
k = 1000, error = -0.000505991
k = 10000, error = -0.000116982

