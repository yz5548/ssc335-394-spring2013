#include <stdio.h>

//void c_multiply_matrix(int* m);
//void fortran_multiply_matrix_(int** m);

main(){
int m[4][4]={{1, 2, 3, 4},{5, 6, 7, 8},{ 9, 10, 11, 12},{ 13, 14, 15, 16}};
printf("calling Fortran subroutine, passing\n");
fortran_multiply_matrix_(&m);
printf("calling C       subroutine, passing\n");
c_multiply_matrix(m);

return 0;
}
