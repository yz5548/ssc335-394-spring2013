#include <stdio.h>

//void c_print_matrix(int*);
//void fortran_print_matrix_(int*);
main(){
//int m[4][4]={1 2 3 4; 5 6 7 8; 9 10 11 12; 13 14 15 16};
int m[4][4]={{1, 2, 3, 4},{5, 6, 7, 8},{ 9, 10, 11, 12},{ 13, 14, 15, 16}};
printf("calling Fortran subroutine, passing\n");
fortran_print_matrix_(&m);
printf("calling C       subroutine, passing\n");
c_print_matrix(m);

return 0;
}
