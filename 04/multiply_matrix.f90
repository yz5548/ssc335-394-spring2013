subroutine fortran_multiply_matrix(m)
	dimension m(4,4)
	integer a(4,4)
	do i=1,4
		do j=1,4
		a(i,j)=m(i,1)*m(1,j)+m(i,2)*m(2,j)+m(i,3)*m(3,j)+m(i,4)*m(4,j)
		end do
	end do
	print '(4(I3,1X))', ((a(i,j),i=1,4),j=1,4)
end subroutine fortran_multiply_matrix
