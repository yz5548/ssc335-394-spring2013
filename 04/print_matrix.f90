subroutine fortran_print_matrix(m)
	!implicit none
	dimension m(4,4)
	!a=m
	print '(4(I2,1X))', ((m(i,j),i=1,4),j=1,4)
end subroutine fortran_print_matrix
