#! /usr/bin/env python

import vtk
import sys

# image reader
filename = sys.argv[1]+".vtk" #"hydrogen.vtk"
reader = vtk.vtkDataSetReader() #reader = vtk.vtkStructuredPointsReader() 
reader.SetFileName(filename)
reader.Update()


# get the extent of the data and print it
W,H,D = reader.GetOutput().GetDimensions()
# string formatting is similar to the sprintf style in C
print "Reading '%s', width=%i, height=%i, depth=%i" %(filename, W, H, D)
reader.Update()
Range = reader.GetOutput().GetScalarRange()#needs Update() before!
print "Range", Range


# create an outline of the dataset
outline = vtk.vtkOutlineFilter()
outline.SetInput( reader.GetOutput() )
outlineMapper = vtk.vtkPolyDataMapper()
outlineMapper.SetInput( outline.GetOutput() )
outlineActor = vtk.vtkActor()
outlineActor.SetMapper( outlineMapper )

# the actors property defines color, shading, line width,...
outlineActor.GetProperty().SetColor(0.0,0.0,1.0)
outlineActor.GetProperty().SetLineWidth(2.0)

#
# creat the isosurface map
isosurface = vtk.vtkContourFilter()
isosurface.SetInput(reader.GetOutput())#isosurface.SetInputConnection(reader.GetOutputPort())
#isosurface.GenerateValues(0.5, 2, 5) #GenerateValues (int numContours, double rangeStart, double rangeEnd). 
isosurface.SetValue(0,Range[1]*0.1)
isosurface.SetValue(1,Range[1]*0.5)
isosurface.SetValue(2,Range[1]*0.9)
isosurface.Update()
isosurfaceMapper = vtk.vtkPolyDataMapper()
isosurfaceMapper.SetInput(isosurface.GetOutput())
isosurfaceActor = vtk.vtkActor()
isosurfaceActor.SetMapper(isosurfaceMapper)
#outlineActor.GetProperty().SetColor(1.0,0.0,0.0)
isosurfaceActor.GetProperty().SetOpacity(0.3)



# renderer and render window 
ren = vtk.vtkRenderer()
ren.SetBackground(.8, .8, .8)
renWin = vtk.vtkRenderWindow()
renWin.SetSize( 400, 400 )
renWin.AddRenderer( ren )

# render window interactor
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow( renWin )

# add the actors
ren.AddActor( outlineActor )
ren.AddActor( isosurfaceActor )
renWin.Render()

# create window to image filter to get the window to an image
w2if = vtk.vtkWindowToImageFilter()
w2if.SetInput(renWin)

# create png writer
wr = vtk.vtkPNGWriter()
wr.SetInput(w2if.GetOutput())

# Python function for the keyboard interface
# count is a screenshot counter
count = 0
def Keypress(obj, event):
    global count, iv
    key = obj.GetKeySym()
    if key == "s":
        renWin.Render()     
        w2if.Modified() # tell the w2if that it should update
        fnm = "%s_%02d.png" %(sys.argv[1],count)
        wr.SetFileName(fnm)
        wr.Write()
        print "Saved '%s'" %(fnm)
        count = count+1
    # add your keyboard interface here
    # elif key == ...

# add keyboard interface, initialize, and start the interactor
iren.AddObserver("KeyPressEvent", Keypress)
iren.Initialize()
iren.Start()