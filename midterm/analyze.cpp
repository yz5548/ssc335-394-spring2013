#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <netcdf.h>
using namespace std;

//***read from netcdf file****
/* Open the file. */
//int ncid;
int main(){
int retval,ncid, status,lat_in,lon_in,temp_in,Wtemp_in,Wspeed_in;
int N=2698;
double lat[N],lon[N],temp[N],Wtemp[N],Wspeed[N];
if ((retval = nc_open("observation_tri.nc", NC_NOWRITE, &ncid)))
      return retval;

/* Query the variable of interest */

if((status = nc_inq_varid (ncid, "lat", &lat_in)))
return retval;
if((status = nc_inq_varid (ncid, "lon", &lon_in)))
return retval;
if((status = nc_inq_varid (ncid, "temp", &temp_in)))
return retval;
if((status = nc_inq_varid (ncid, "Wtemp", &Wtemp_in)))
return retval;
if((status = nc_inq_varid (ncid, "Wspeed", &Wspeed_in)))
return retval;
if((status = nc_inq_varid (ncid, "lat", &lat_in)))
return retval;
if((status = nc_inq_varid (ncid, "lon", &lon_in)))
return retval;
if((status = nc_inq_varid (ncid, "temp", &temp_in))) 
return retval;
if((status = nc_inq_varid (ncid, "Wtemp", &Wtemp_in))) 
return retval;
if((status = nc_inq_varid (ncid, "Wspeed", &Wspeed_in))) 
return retval;
/* Read the data. */
if ((retval = nc_get_var_double(ncid, lat_in, lat)))
    return retval;
if ((retval = nc_get_var_double(ncid, lon_in, lon)))
    return retval;
if ((retval = nc_get_var_double(ncid, temp_in,temp)))
    return retval;
if ((retval = nc_get_var_double(ncid, Wtemp_in,Wtemp)))
    return retval;
if ((retval = nc_get_var_double(ncid, Wspeed_in,Wspeed)))
    return retval;
/* Do something useful with the data… */
   
/* Close the file. */
   if ((retval = nc_close(ncid)))
      return retval;
// *************test ouput*****************
/*
for (int i=0;i<N;++i){
cout<<lat[i]<<" "<<lon[i]<<" "<<temp[i]<<" "<<Wtemp[i]<<" "<<Wspeed[i]<<endl;
}
*/


//calculate the min lat/lon and max lat/lon
double lat_max,lat_min,lon_max,lon_min;
lat_max=lat[0];lat_min=lat[0];lon_max=lon[0];lon_min=lon[0];
for (int i=0;i<N;i++){
if (lat[i]>lat_max) lat_max=lat[i];
if (lat[i]<lat_min) lat_min=lat[i];
if (lon[i]>lon_max) lon_max=lon[i];
if (lon[i]<lon_min) lon_min=lon[i];
}
cout<<"The geographic extent of the data"<<endl;
cout<<"The maximum latitude:  "<<lat_max<<endl;
cout<<"The minimum latitude:  "<<lat_min<<endl;
cout<<"The maximum longitude: "<<lon_max<<endl;
cout<<"The minimum longitude: "<<lon_min<<endl;
//calculate the midpoint of lat and lon, seprate the data into 4 bins
double lat_m,lon_m;
lat_m=(lat_max+lat_min)*.5;
lon_m=(lon_max+lon_min)*.5;
cout<<"The midpoint of the data is(lat,lon): ("<<lat_m<<","<<lon_m<<")"<<endl;
//calculate the average of (temp, Wtemp, Wspeed) within each bin
//NE(0) region lat>=lat_m && lon>=lon_m
//NW(1) region lat>=lat_m && lon<=lon_m
//SE(2) region lat<lat_m && lon>lon_m
//SW(3) region lat<lat_m && lon<lon_m
double Mtemp[4],MWtemp[4],MWspeed[4];
double sum[3]={0.0};int k[3]={0};
//NE(0) region lat>=lat_m && lon>=lon_m
for (int i=0;i<=N;i++){
	if (lat[i]>=lat_m && lon[i]>=lon_m && temp[i]!=-99999999){
	//cout<<temp[i]<<endl;
	sum[0]=sum[0]+temp[i];
	k[0]=k[0]+1;
	}
	if (lat[i]>=lat_m && lon[i]>=lon_m && Wtemp[i]!=-99999999){
	sum[1]=sum[1]+Wtemp[i];
	k[1]=k[1]+1;
	}
	if (lat[i]>=lat_m && lon[i]>=lon_m && Wspeed[i]!=-99999999){
	sum[2]=sum[2]+Wspeed[i];
	k[2]=k[2]+1;
	}
}
Mtemp[0]=sum[0]/k[0];
MWtemp[0]=sum[1]/k[1];
MWspeed[0]=sum[2]/k[2];
cout<<"In NE:"<<endl;
cout<<"Average Temperature:      "<<Mtemp[0]<<"F"<<endl;
cout<<"Average Water Temperature:"<<MWtemp[0]<<"F"<<endl;
cout<<"Average Wind Speed:       "<<MWspeed[0]<<"mph"<<endl;
/*

*/

//NW(1) region lat>=lat_m && lon<=lon_m
sum[0]=0.0;sum[1]=0.0;sum[2]=0.0;
  k[0]=0;    k[1]=0;    k[2]=0;  
for (int i=0;i<=N;i++){
	if (lat[i]>=lat_m && lon[i]<=lon_m && temp[i]!=-99999999){
	sum[0]=sum[0]+temp[i];
	k[0]=k[0]+1;
	}
	if (lat[i]>=lat_m && lon[i]<=lon_m && Wtemp[i]!=-99999999){
	sum[1]=sum[1]+Wtemp[i];
	k[1]=k[1]+1;
	}
	if (lat[i]>=lat_m && lon[i]<=lon_m && Wspeed[i]!=-99999999){
	sum[2]=sum[2]+Wspeed[i];
	k[2]=k[2]+1;
	}
}
Mtemp[1]=sum[0]/k[0];
MWtemp[1]=sum[1]/k[1];
MWspeed[1]=sum[2]/k[2];
cout<<"In NW:"<<endl;
cout<<"Average Temperature:      "<<Mtemp[1]<<"F"<<endl;
cout<<"Average Water Temperature:"<<MWtemp[1]<<"F"<<endl;
cout<<"Average Wind Speed:       "<<MWspeed[1]<<"mph"<<endl;
//SE(2) region lat<lat_m && lon>lon_m	
sum[0]=0.0;sum[1]=0.0;sum[2]=0.0;
  k[0]=0;    k[1]=0;    k[2]=0;  
for (int i=0;i<=N;i++){
	if (lat[i]<=lat_m && lon[i]>=lon_m && temp[i]!=-99999999){
	sum[0]=sum[0]+temp[i];
	k[0]=k[0]+1;
	}
	if (lat[i]<=lat_m && lon[i]>=lon_m && Wtemp[i]!=-99999999){
	sum[1]=sum[1]+Wtemp[i];
	k[1]=k[1]+1;
	}
	if (lat[i]<=lat_m && lon[i]>=lon_m && Wspeed[i]!=-99999999){
	sum[2]=sum[2]+Wspeed[i];
	k[2]=k[2]+1;
	}
}
Mtemp[2]=sum[0]/k[0];
MWtemp[2]=sum[1]/k[1];
MWspeed[2]=sum[2]/k[2];
cout<<"In SE:"<<endl;
cout<<"Average Temperature:      "<<Mtemp[2]<<"F"<<endl;
cout<<"Average Water Temperature:"<<MWtemp[2]<<"F"<<endl;
cout<<"Average Wind Speed:       "<<MWspeed[2]<<"mph"<<endl;

//SW(3) region lat<lat_m && lon<lon_m
sum[0]=0.0;sum[1]=0.0;sum[2]=0.0;
  k[0]=0;    k[1]=0;    k[2]=0;  
for (int i=0;i<=N;i++){
	if (lat[i]<=lat_m && lon[i]<=lon_m && temp[i]!=-99999999){
	sum[0]=sum[0]+temp[i];
	k[0]=k[0]+1;
	}
	if (lat[i]<=lat_m && lon[i]<=lon_m && Wtemp[i]!=-99999999){
	sum[1]=sum[1]+Wtemp[i];
	k[1]=k[1]+1;
	}
	if (lat[i]<=lat_m && lon[i]<=lon_m && Wspeed[i]!=-99999999){
	sum[2]=sum[2]+Wspeed[i];
	k[2]=k[2]+1;
	}
}
Mtemp[3]=sum[0]/k[0];
MWtemp[3]=sum[1]/k[1];
MWspeed[3]=sum[2]/k[2];
cout<<"In SW:"<<endl;
cout<<"Average Temperature:      "<<Mtemp[3]<<"F"<<endl;
cout<<"Average Water Temperature:"<<MWtemp[3]<<"F"<<endl;
cout<<"Average Wind Speed:       "<<MWspeed[3]<<"mph"<<endl;




/*
	if (lat[i]>=lat_m && lon[i]<=lon_m && temp[i]!=-99999999){
	sum[0]=sum[0]+temp[i];
	k[0]=k[0]+1;
	}
	if (lat[i]>=lat_m && lon[i]<=lon_m && Wtemp[i]!=-99999999){
	sum[1]=sum[1]+Wtemp[i];
	k[1]=k[1]+1;
	}
	if (lat[i]>=lat_m && lon[i]<=lon_m && Wspeed[i]!=-99999999){
	sum[2]=sum[2]+Wspeed[i];
	k[2]=k[2]+1;
	}
	*/

/*
double aa[4]={10};
aa[4]={0.0};
for (int i=0;i<4;i++){
cout<<aa[i]<<endl;
}
*/



return 0;
}
