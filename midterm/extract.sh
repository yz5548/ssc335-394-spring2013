#!/bin/bash
outputfile="observation.csv"
if [ -f $outputfile ]; then
rm $outputfile
fi

echo "Location,StationID,Latitude,Longitude,Temperature(F),Water Temperature(F),Wind Direction,Wind Speed(mph)" > $outputfile

#filename="SH118.xml"  #"0Y2W3.xml"
ls xml/*.xml > filelist
#for f in '*.xml';do
while read line;do
filename=$line;
echo $(grep '<location>' $filename | cut -f2 -d">"|cut -f1 -d"<" | tr -d ',') \
',' $(grep '<station_id>' $filename | cut -f2 -d">"|cut -f1 -d"<") \
',' $(grep '<latitude>' $filename | cut -f2 -d">"|cut -f1 -d"<") \
',' $(grep '<longitude>' $filename | cut -f2 -d">"|cut -f1 -d"<") \
',' $(grep '<temp_f>' $filename | cut -f2 -d">"|cut -f1 -d"<") \
',' $(grep '<water_temp_f>' $filename | cut -f2 -d">"|cut -f1 -d"<") \
',' $(grep '<wind_degrees>' $filename | cut -f2 -d">"|cut -f1 -d"<") \
',' $(grep '<wind_mph>' $filename | cut -f2 -d">"|cut -f1 -d"<") >> $outputfile ;
done <filelist
grep -v "Unknown Station" $outputfile > observation_tri.csv
