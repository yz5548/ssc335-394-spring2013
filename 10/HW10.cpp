#include<stdio.h>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

typedef struct point_t {
  double x, y, z;
} point_t;

typedef struct cell_t {
  point_t v0, v1, v2;
} cell_t;

int main(int argc, char** argv){
    const char* FILENAME = argv[1];
    ifstream fin(FILENAME);  
    int NODE_SIZE; 	
    string str;

    /* Read Mesh + Physical */
    while(str != "$Nodes"){
	getline(fin,str);
    }

    /* Read Nodes */
    fin >> NODE_SIZE; // read number of nodes
    //cout<< NODE_SIZE<<endl;
    point_t node[NODE_SIZE+1];   
    double nodeXmax, nodeXmin, nodeYmax, nodeYmin, aspect;
    nodeXmax=-9999999999;nodeXmin=9999999999;
    nodeYmax=-9999999999;nodeYmin=9999999999;
    for (int i = 0; i < NODE_SIZE; ++i){   
	int index;
	fin >> index;
	fin >> node[index].x >> node[index].y>> node[index].z; // read  the line 
	if (nodeXmax<node[index].x ){
		nodeXmax=node[index].x;
	}
       if (nodeYmax<node[index].y ){
		nodeYmax=node[index].y;
	}
	if (nodeXmin>node[index].x ){
		nodeXmin=node[index].x;
	}
       if (nodeYmin>node[index].y ){
		nodeYmin=node[index].y;
	}
	//cout<< index << " "<<node[index].x <<" "<< node[index].y<<" "<< node[index].z << endl; 
    }
   //calculate aspect ratio (height / width)
      aspect=(nodeYmax-nodeYmin)/(nodeXmax-nodeXmin);

    /* Read Elements */ 
    while(str != "$Elements"){
	getline(fin,str);
    }
    
    int ELEMENT_SIZE;
    fin >> ELEMENT_SIZE;// read number of elements
    //cout << ELEMENT_SIZE<<endl; 
    cell_t cell[ELEMENT_SIZE+1];
    double area_max,area_min;
    area_max=-9999999999;area_min=9999999999;
    for (int i = 0; i < ELEMENT_SIZE; ++i){   
	int i0, i1, i2, index, style, numoftag, tag1, tag2;
	fin >> index >> style;
	//cout << index << " "<< style <<" ";
	switch (style){
	case 15:
		fin >> numoftag >> tag1 >>tag2>> i0; 
		//cout<< numoftag << " " << tag1 << " " << tag2 << " "<< i0 <<endl;
		//cell[index].v0 = node[i0];
		break;
	case 1:
		fin >> numoftag>> tag1 >>tag2>> i0 >> i1;
		//cout << numoftag << " " << tag1 << " " << tag2 << " "<< i0 << " " << i1<<endl;
		//cell[index].v0 = node[i0];
		//cell[index].v1 = node[i1];
		break;
	case 2:
		fin >> numoftag>> tag1 >>tag2>> i0 >> i1>> i2;  
		//cout << numoftag << " " << tag1 << " " << tag2 << " "<< i0 << " " << i1 <<" "<< i2<<endl;
		cell[index].v0 = node[i0];
		cell[index].v1 = node[i1];
		cell[index].v2 = node[i2];
		//bulid vertor for the triangle
		double a[3], b[3], area;
		a[0]=cell[index].v1.x-cell[index].v0.x;
		a[1]=cell[index].v1.y-cell[index].v0.y;
		a[2]=cell[index].v1.z-cell[index].v0.z;
		b[0]=cell[index].v2.x-cell[index].v0.x;
		b[1]=cell[index].v2.y-cell[index].v0.y;
		b[2]=cell[index].v2.z-cell[index].v0.z;
		//calculate area
		area=(a[1]*b[2]+b[0]*a[2]+a[0]*b[1]-b[1]*a[2]-a[0]*b[2]-b[0]*a[1])/2.0;
		if (area<=0){
		cout <<"Area can not be smaller than or equal to 0"<<endl;
		}
		if (area<area_min){
			area_min=area;
		}
		if (area>area_max){
			area_max=area;
		}
/*
			cout << "("<< cell[index].v0.x << " "<< cell[index].v0.y << " "<<cell[index].v0.z<<"), "<<\
			"("<< cell[index].v1.x << " "<< cell[index].v1.y << " "<<cell[index].v1.z<<"), "<<\
			"("<< cell[index].v2.x << " "<< cell[index].v2.y << " "<<cell[index].v2.z<<"), "<<endl;
*/
		break;	
	}
    }	
   fin.close();

   	ofstream fout("mesh_statisics.csv");
 	//write to csv file: mesh lename, number of elements, max area, min area, aspect ratio (height/width).
	fout << "mesh filename, "<< "number of elements,"<< "max area,"<<" min area,"<<" aspect ratio(height/width)"<<endl;
	fout <<FILENAME<<","<<ELEMENT_SIZE<<","<<area_max<<","<<area_min<<","<<aspect<<endl;
	//print to screen
	cout << "mesh filename, "<< "number of elements,"<< "max area,"<<" min area,"<<" aspect ratio(height/width)"<<endl;
	cout <<FILENAME<<","<<ELEMENT_SIZE<<","<<area_max<<","<<area_min<<","<<aspect<<endl;
	cout << "The above informations have been written to file mesh_statisics.csv"<<endl;
	fout.close();
    return 0;
}
