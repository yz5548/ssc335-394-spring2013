#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "mmio.h"
#include "read_mm.h"

/** Forms the LU decomposition of A
 *
 *  A - Matrix to be given LU (input)
 *  L - Lower triangular matrix (output)
 *  U - Upper triangular matrix (output)
 *  M - number of rows and columns (input)
 */
int LU_decomp(double* A, double *L, double *U, int M){
//Row-major order is used in C, PL/I, Python and others. 
//Column-major order is used in Fortran, MATLAB, GNU Octave, R, Rasdaman, X10 and Scilab.
//********** LU decomposition *****//
 int i,k,j,p;
 double sum,a[M][M],u[M][M],l[M][M];
 /*wrong example
  double u[10][M]={0}; error: variable-sized object may not be initialized
  double u[10][10]={0}; good
 */
 //read the array
 k=0;
  for (i=0;i<M;i++){
	 for (j=0;j<M;j++){
	 a[i][j]=*(A+k);
	 u[i][j]=0;
	 l[i][j]=0;
	 k=k+1;
	 }
 }
 //begin LU decomposition 
 for(k=0;k<M;k++)
    {
        u[k][k]=1;
        for(i=k;i<M;i++)
        {
            sum=0;
            for(p=0;p<k;p++)
                sum+=l[i][p]*u[p][k];
            l[i][k]=a[i][k]-sum;
        }

        for(j=k+1;j<M;j++)
        {
            sum=0;
            for(p=0;p<k;p++)
                sum+=l[k][p]*u[p][j];
            u[k][j]=(a[k][j]-sum)/l[k][k];
        }
    }
	//**********Modify the physical address value **********//
	k=0;
	for (i=0;i<M;i++){
		for (j=0;j<M;j++){
		*(U+k)=u[i][j];
		*(L+k)=l[i][j];
		k=k+1;
		}
	}
    //******** Displaying LU matrix**********//
	/*
	printf("The original matrix is:\n");
	for(i=0;i<M;i++)
    {
        for(j=0;j<M;j++)
            printf("%4.2f ",a[i][j]);//cout<<U[i][j]<<"  ";
        printf("\n");
    }
	printf("LU matrix is: \n");
	printf("L:\n");
    for(i=0;i<M;i++)
    {
        for(j=0;j<M;j++)
			printf("%4.2f ",l[i][j]);
            printf("\n");
    }
  printf("U:\n");
    for(i=0;i<M;i++)
    {
        for(j=0;j<M;j++)
            printf("%4.2f ",u[i][j]);//cout<<U[i][j]<<"  ";
        printf("\n");
    }
	*/
  return 0;
}

/** Prints the error ||A - L*U|| */
int compare_A_to_LU(double *A, double *L, double *U, int M){
  int ndiff=0;
  int i,j,k;
  double l2_error = 0.0;
  //printf("%d\n",M);
  double u[M][M],l[M][M],a[M][M],lu[M][M];
  //read the data
  k=0;
  for(i=0;i<M;i++){
	 for(j=0;j<M;j++){
	 a[i][j]=*(A+k);
	 u[i][j]=*(U+k);
	 l[i][j]=*(L+k);
	 //printf("%4.2f ",l[i][j]);
	 lu[i][j]=0; //initialize
	 k=k+1;
	 	 }
 }
 //printf("\n");
  //L*U
   for(i=0;i<M;i++){
	 for(j=0;j<M;j++){
		for(k=0;k<M;k++){
		lu[i][j]=lu[i][j]+l[i][k]*u[k][j];
		}
		//printf("%4.2f ",lu[i][j]);
	 }
	}
	 //printf("\n");
 //the error
  for (i=0;i<M;i++){
	 for (j=0;j<M;j++){
	 l2_error =l2_error+(a[i][j]-lu[i][j])*(a[i][j]-lu[i][j]);
	 if ((a[i][j]-lu[i][j])!=0)	 ndiff=ndiff+1;
	 }
  }
  printf("Error in A - LU, num diff entries: %d, l2 error: %.2e\n", ndiff, sqrt(l2_error));
  return ndiff;
}

/** Forward solve (Ly = b where y = Ux)
 * 
 * L - lower trianguler M X M matrix (input)
 * y - solution vector (MX1) (output)
 * b - right hand side vector (MX1) (input)
 * M - system size (input)
 */
int forward_solve(double* L, double* y, double *b, int M)
{
  double l[M][M],B[M],Y[M];
  int i,j,k,tmp;
  //read the data and reformat
  k=0;
  for(i=0;i<M;i++){
	B[i]=*(b+i);
	 for(j=0;j<M;j++){
	 l[i][j]=*(L+k);
	 k=k+1;
	 }
  }
  //solve the Y
  Y[0]=B[0]/l[0][0];
  //for(i=M-1,i>=0, i--){
  for(i=1;i<M;i++){
  tmp=0;
  for(j=0;j<=i-1;j++){
  tmp=tmp+l[i][j]*Y[j];
  }
  	Y[i]=(B[i]-tmp)/l[i][i];
  }
  //for(i=0;i<M;i++) printf("%4.2f ",Y[i]); printf("\n");

  //change the value in y's memory address
   for(i=0;i<M;i++){
   *(y+i)=Y[i];
   }
  return 0;
}

/** Backward solve (Ux = y)
 * 
 * U - backward trianguler M X M matrix (input)
 * x - solution vector (MX1) (output)
 * y - right hand side vector (MX1) (input)
 * M - system size (input)
 */
int backward_solve(double* U, double* x, double *y, int M)
{
double u[M][M],X[M],Y[M];
  int i,j,k,tmp;
  //read the data and reformat
  k=0;
  for(i=0;i<M;i++){
	Y[i]=*(y+i);
	 for(j=0;j<M;j++){
	 u[i][j]=*(U+k);
	 k=k+1;
	 }
  }
  //solve the Y
  X[0]=Y[M-1]/u[M-1][M-1];
  //for(i=M-1,i>=0, i--){
  for(i=M-1;i>=0;i--){
  tmp=0;
  for(j=i+1;j<=M-1;j++){
  tmp=tmp+u[i][j]*X[j];
  }
  	X[i]=(Y[i]-tmp)/u[i][i];
  }
  //for(i=0;i<M;i++) printf("%4.2f ",X[i]); printf("\n");

  //change the value in y's memory address
   for(i=0;i<M;i++){
   *(x+i)=X[i];
   }
  return 0;
}

/** Solves the Ax = b
 *
 *  A - given MXM matrix
 *  x - solution vector (MX1) to output
 *  b - right hand side (MX1) vector
 *  M - size of system
 */
int matrix_solver(double* A, double* x, double* b, int M)
{
  double *L, *U, *y, l2_norm;
  int i;
  l2_norm=0.0;

  L = (double*) malloc(M * M * sizeof(double));
  U = (double*) malloc(M * M * sizeof(double));
  LU_decomp(A, L, U, M);
  y = (double*) malloc(M * sizeof(double));  
  /* ... */
  forward_solve(L, y, b, M); //(Ly = b where y = Ux)
  //for(i=0;i<M;i++) printf("%4.2f ",y[i]); printf("\n");
  backward_solve(U, x, y, M); //(Ux = y)
  //for(i=0;i<M;i++) printf("%4.2f ",x[i]); printf("\n");
  for (i = 0; i < M; ++i) l2_norm += x[i]*x[i];
  printf("L2 norm of solver: %f\n", sqrt(l2_norm));
  free(L);
  free(U);
  free(y);
  return 0;
}



/** Test the LU Routine
 */
int test_matrix_solver(){
  double A[9] = {1, 2, 3, 2, -4, 6, 3, -9, -3};
  double L[9], U[9], x[3];
  double b[3] = {5, 18, 6};
  double error;
  int p;
  int i,j,k,M;
  M=3;
  /*
   for (p=0;p<9;p++){
   printf("%4.2f ",A[p]);
   }
     printf("\n");
  */
  printf("This is a test:\n");
  LU_decomp(A, L, U, 3);
    //******** Displaying LU matrix**********//
  /*
	printf("Outside the LU function:\n");
	printf("The original matrix is:\n");
	k=0;
	for(i=0;i<M;i++)
    {
        for(j=0;j<M;j++){
            printf("%4.2f ",A[k]);//cout<<U[i][j]<<"  ";
			k=k+1;
		}
        printf("\n");
    }
	printf("LU matrix is: \n");
	printf("L:\n");
	k=0;
    for(i=0;i<M;i++)
    {
        for(j=0;j<M;j++){
			printf("%4.2f ",L[k]);
			k=k+1;
		}
        printf("\n");
    }
  printf("U:\n");
  k=0;
    for(i=0;i<M;i++)
    {
        for(j=0;j<M;j++){
            printf("%4.2f ",U[k]);//cout<<U[i][j]<<"  ";
			k=k+1;
		}
        printf("\n");
	}
	*/
  compare_A_to_LU(A, L, U, 3);
  /* ... */
  matrix_solver(A, x, b, 3);
  /* print comparison to test */
  double sol[3] = {1, -1, 2};
  error=0;
  for (i=0;i<M;i++){
  error=error+(sol[i]-x[i])*(sol[i]-x[i]);
  }
  printf("Error in test: %.e\n", error);
  printf("The computed solution of this test equation is (format: x1 x2 x3):\n");
  for(i=0;i<M;i++) printf("%4.2f ",x[i]); printf("\n");
  printf("The exact solution of this test equation is:\n");
  for(i=0;i<M;i++) printf("%4.2f ",sol[i]); printf("\n");
 // printf("Error in test: %.e\n", error);
  printf("The difference is (exact-computed):\n");
  for(i=0;i<M;i++) printf("%4.2f ",sol[i]-x[i]); printf("\n");
  return 0;
}

int main(int argc, char** argv)
{
  int ret_code, M, N, i;
  double *A=NULL, *x, *b;
  FILE *f;

  if (argc < 2) {
    fprintf(stderr, "Usage: %s [martix-market-filename | test]\n", argv[0]);
    return 1;
  }

  if (!strcmp(argv[1], "test")) {
    return test_matrix_solver();
  }

  if ((f = fopen(argv[1], "r")) == NULL) {
    printf("Could not open file.\n");
    return 1;
  }

  ret_code = read_mm(f, &A, &M, &N);
  if (ret_code != 0){
    printf("Error opening Matrix Market file\n");
    return 1;
  } else {
    x = (double*) malloc(M*sizeof(double));
    b = (double*) malloc(M*sizeof(double));
    for (i=0; i<M; ++i) b[i] = 1;
    ret_code = matrix_solver(A, x, b, M);
    free(x);
    free(b);
    return ret_code;
  }
  return 0;
}
