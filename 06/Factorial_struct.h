#include<stdio.h>
#include<limits.h>
unsigned int f,f0,A1[2],A[2];
Factorial_struct(int n){
unsigned int (*T)[2];
T=calloc(2,sizeof(unsigned int));
int i,bp;//bp stands for breakpoint
f=1;bp=0;
A1[0]=0;A1[1]=0;
A[0]=0;A[1]=0;
//printf("%d\n",bp);
(*T)[0]=0;(*T)[1]=0;
for(i=2;i<=n;i++){
	f0=f;
	f=i*f;
	//printf("i=%d,f=%u\n",i,f);
	if (f<i || f<f0 || (UINT_MAX/f0)<(i+1)){
	bp=i;
	printf("  The %d! starts exceed the UINT_MAX\n",i);
	printf("           the following calculation will use MultiplyBigInt_struct()\n");
	printf("           the number will be stored in the a*BIG_INT+b format()\n");
	A1[0]=0;A1[1]=f0;
	break;
	}	
}
printf("      This following algorithm is correct but not optimized, it takes a little bit longer\n");
printf("      Please waiting..........\n");
for(i=bp;i<=n;i++){
T=MultiplyBigInt_struct(A1,i);
//A1[0]=A2[0];A1[1]=A2[1];
A1[0]=(*T)[0];A1[1]=(*T)[1];
}

printf("The %d! is %u*BIG_INT+%u,while BIG_INT=%u\n",n,(*T)[0],(*T)[1],UINT_MAX);


}

