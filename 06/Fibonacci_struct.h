#include<stdio.h>
#include<limits.h>
unsigned int f1,f2,f,A1[2],A2[2],A[2];
Fibonacci_struct(int n){
//printf("starting Fibonacci_struct\n");
//printf("Good\n");
unsigned int (*T)[2];
T=calloc(2,sizeof(unsigned int));
int i,bp;//bp stands for breakpoint
f1=0;f2=1;bp=0;
A1[0]=0;A1[1]=0;
A2[0]=0;A2[1]=0;
A[0]=0;A[1]=0;
//printf("%d\n",bp);
(*T)[0]=0;(*T)[1]=0;
for(i=3;i<=n;i++){
	f=f1+f2;
	if (f<f1 || f<f1){
	bp=i;
	printf("  The %dth Fibonacci starts exceed the UINT_MAX\n",i);
	printf("           the following calculation will use SumBigInt_struct()\n");
	printf("           the number will be stored in the a*BIG_INT+b format()\n");
	A1[0]=0;A1[1]=f1;
	A2[0]=0;A2[1]=f2;//A2[1]=f2-(UINT_MAX-f1);//a1=A1[0],b1=A1[1],f=a*UINT_MAX+b
	break;
	}	
	f1=f2;f2=f;
}

for(i=bp;i<=n;i++){
T=SumBigInt_struct(A1,A2);
A1[0]=A2[0];A1[1]=A2[1];
A2[0]=(*T)[0];A2[1]=(*T)[1];
}

printf("The %dth Fibonacci is %u*BIG_INT+%u,while BIG_INT=%u\n",n,(*T)[0],(*T)[1],UINT_MAX);


}
