#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"SumBigInt.h"
#include"SumBigInt_struct.h"
#include"MultiplyBigInt_struct.h"

#include"Fibonacci.h"
#include"Fibonacci_struct.h"

#include"Factorial.h"
#include"Factorial_struct.h"

//char* Fibonacci(int n);
int main(){

//Test with Fibonacci
printf("Fibonacci Test with struct(a*BIG_INT+b)  method\n");
Fibonacci_struct(50);
printf("\n");

printf("Factorial Test with struct(a*BIG_INT+b) method\n");
Factorial_struct(15);
printf("\n");

printf("Fibonacci Test with string method\n");
//Fibonacci(48);
//Fibonacci(49);
Fibonacci(50);
printf("\n");

printf("Factorial Test with int array method\n");
/*
Factorial(12);
Factorial(13);
Factorial(14);
*/
Factorial(15);

return 0;

}

