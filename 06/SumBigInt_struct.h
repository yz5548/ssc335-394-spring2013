#include<stdio.h>
#include <stdlib.h> 
#include<limits.h>
//unsigned int (*SumBigInt_struct(unsigned int *A1, unsigned int *A2))[2]{
unsigned int (*SumBigInt_struct(unsigned int A1[2], unsigned int A2[2]))[2]{
unsigned int a1,b1,a2,b2,a,b;
unsigned int (*A)[2];
A=calloc(2,sizeof(unsigned int));
a1=A1[0];b1=A1[1];
a2=A2[0];b2=A2[1];
//a1=*A1;b1=++*A1; //incorrect
//a2=*A2;b2=++*A2;
//printf("a1=%u,b1=%u\n",a1,b1);
//printf("a2=%u,b2=%u\n",a2,b2);
//assume a1+a2<=UINT_MAX
a=a1+a2;
b=b1+b2;
if (b<b1 || b<b2){ //ensure b<min(b1,b2) 
/* use b<min(b1,b2) to define overflow; 
   no problem at calculating Fibonnaci
   protential problem at factorical when the number increase several UNIT_MAX at one operation
*/
a=a+1;
b=b2-(UINT_MAX-b1);
}

//printf("a=%u,b=%u\n",a,b);
//printf("everything is OK until here No.1\n");
(*A)[0]=a;(*A)[1]=b;
//printf("a=%u,b=%u\n",(*A)[0],(*A)[1]);
//printf("everything is OK until here No.2\n");
return A;
}
