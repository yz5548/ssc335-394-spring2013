#!/bin/bash
#Average method error
filename="output_10times.txt"
if [ -f Avgmethoderror.txt ]; then
rm Avgmethoderror.txt
fi
echo 'Average method error'
echo 'Average method error'> Avgmethoderror.txt
m="Midpoint,Trapezoid,Simpson"
#***seperate m****
OLD_IFS="$IFS"
IFS=","
arr=($m)
IFS="$OLD_IFS"
#*****************
for k in ${arr[@]}
do
echo $k' average error =' $(grep -A 4 $k output_10times.txt | grep k |awk '{sum+=$6} END {print sum/FNR}') >> Avgmethoderror.txt
done
echo 'the output has been printed to Avgmethoderror.txt'
