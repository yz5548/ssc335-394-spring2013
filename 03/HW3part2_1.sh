#!/bin/bash
#Average k error
filename="output_10times.txt"
if [ -f "$filename" ]; then
rm Avgkerror.txt
fi
echo 'Average k error'
echo 'Average k error'>Avgkerror.txt
for k in 10 100 1000 10000
do
echo 'k ='$k', average error =' $(grep -a $k, $filename |awk '{sum+=$6} END {print sum/FNR}') >> Avgkerror.txt
done
echo 'the output has been printed to Avgkerror.txt'
