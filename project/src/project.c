/*Compile info
To use the GSL library, compile the source code with the option:
        -I$TACC_GSL_INC -I$TACC_GSL_INC/gsl
and add the following commands to the link step: 
        -L$TACC_GSL_LIB -lgsl -lgslcblas
//	for netcdf
To use the NETCDF library, compile the source code with the option:
        -I${TACC_NETCDF_INC} 
and add the following options to the link step: 
        -L${TACC_NETCDF_LIB} -lnetcdf -L${TACC_HDF5_LIB} -lhdf5_hl -lhdf5 -lz -lm
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netcdf.h> //using netcdf library
#include <gsl/gsl_multifit.h> //using gsl library
#include <gsl/gsl_statistics.h>


int main(void){
//read the netcdf file
int retval, ncid, status, lat_in, lon_in, time_in;
int phi500_in, W_in, t700_td_in, spei_in;
int n_lat, n_lon, n;
int i,j,k,nn;
int N=769;n_lat=73, n_lon=144;
double lat[N], lon[N], time[N];
double phi500[N][n_lat][n_lon], W[N][n_lat][n_lon], t700_td[N][n_lat][n_lon], spei[N-13][n_lat][n_lon];
double phi500_x[N*n_lat*n_lon], W_x[N*n_lat*n_lon], t700_td_x[N*n_lat*n_lon], spei_x[(N-13)*n_lat*n_lon];
/*read the data*/
//open the file phi500.nc
if ((retval = nc_open("ncfile/phi500.nc", NC_NOWRITE, &ncid))) return retval;

if((status = nc_inq_varid (ncid, "phi500", &phi500_in))) return retval;
// Read the data. 
if ((retval = nc_get_var_double(ncid, phi500_in,phi500_x))) return retval;
// Close the file. 
if ((retval = nc_close(ncid)))  return retval;


//check the demension is read correct or not
//for (i=0;i<n_lat;i++) printf("%e\n",lat[i]); 
//for (i=0;i<n_lon;i++) printf("%e\n",lon[i]);
//for (i=0;i<N;i++) printf("%e\n",time[i]);

//open the file W.nc
if ((retval = nc_open("ncfile/W.nc", NC_NOWRITE, &ncid))) return retval;
// Query the variable of interest 
if((status = nc_inq_varid (ncid, "lat", &lat_in))) return retval;
if((status = nc_inq_varid (ncid, "lon", &lon_in))) return retval;
if((status = nc_inq_varid (ncid, "time", &time_in))) return retval;
if((status = nc_inq_varid (ncid, "W", &W_in))) return retval; // Query the variable of interest 
// Read the data. 
if ((retval = nc_get_var_double(ncid, lat_in, lat))) return retval;
if ((retval = nc_get_var_double(ncid, lon_in, lon))) return retval;
if ((retval = nc_get_var_double(ncid, time_in,time))) return retval;
if ((retval = nc_get_var_double(ncid, W_in,W_x))) return retval; // Read the data. 
if ((retval = nc_close(ncid)))  return retval; // Close the file. 
//printf("W:\n");
//for (k=0;k<2;k++) printf("%e\n",W_x[k]);//check the W is read correctly or not

//open the file t700_td.nc
if ((retval = nc_open("ncfile/t700_td.nc", NC_NOWRITE, &ncid))) return retval;
if((status = nc_inq_varid (ncid, "t700_td", &t700_td_in))) return retval; // Query the variable of interest 
if ((retval = nc_get_var_double(ncid, t700_td_in,t700_td_x))) return retval; // Read the data. 
if ((retval = nc_close(ncid)))  return retval; // Close the file. 
//open the file spei.nc
if ((retval = nc_open("ncfile/spei.nc", NC_NOWRITE, &ncid))) return retval;
if((status = nc_inq_varid (ncid, "spei", &spei_in))) return retval; // Query the variable of interest 
if ((retval = nc_get_var_double(ncid, spei_in,spei_x))) return retval; // Read the data. 
if ((retval = nc_close(ncid)))  return retval; // Close the file. 
//printf("spei:\n");
//for (k=0;k<3;k++) printf("%e\n",spei_x[k]);//check the spei is read correctly or not
//convert the demension
nn=0;
for (k=0;k<N;k++){
	for (i=0;i<n_lat;i++){
		for(j=0; j<n_lon;j++){
			phi500[k][i][j]=phi500_x[nn]; 
			W[k][i][j]=W_x[nn];
			t700_td[k][i][j]=t700_td_x[nn];
			spei[k][i][j]=spei_x[nn];
			nn=nn+1;
		}
	}
}
nn=0;
for (k=0;k<N-13;k++){
	for (i=0;i<n_lat;i++){
		for(j=0; j<n_lon;j++){
			spei[k][i][j]=spei_x[nn];
			nn=nn+1;
		}
	}
}
//for (k=0;k<10;k++) printf("%e\n",spei[k][0][0]);//check!!
//read the climatology
double phi500_mean_x[12*n_lat*n_lon], W_mean_x[12*n_lat*n_lon], t700_td_mean_x[12*n_lat*n_lon];
double phi500_mean[12][n_lat][n_lon], W_mean[12][n_lat][n_lon], t700_td_mean[12][n_lat][n_lon];
//open the file W.nc
if ((retval = nc_open("ncfile/phi500_climatology.nc", NC_NOWRITE, &ncid))) return retval;
if((status = nc_inq_varid (ncid, "phi500_mean", &phi500_in))) return retval; // Query the variable of interest 
if ((retval = nc_get_var_double(ncid, phi500_in, phi500_mean_x))) return retval; // Read the data. 
if ((retval = nc_close(ncid)))  return retval; // Close the file. 
//open the file W.nc
if ((retval = nc_open("ncfile/W_climatology.nc", NC_NOWRITE, &ncid))) return retval;
if((status = nc_inq_varid (ncid, "W_mean", &W_in))) return retval; // Query the variable of interest 
if ((retval = nc_get_var_double(ncid, W_in,W_mean_x))) return retval; // Read the data. 
if ((retval = nc_close(ncid)))  return retval; // Close the file. 
//open the file t700_td.nc
if ((retval = nc_open("ncfile/t700_td_climatology.nc", NC_NOWRITE, &ncid))) return retval;
if((status = nc_inq_varid (ncid, "t700_td_mean", &t700_td_in))) return retval; // Query the variable of interest 
if ((retval = nc_get_var_double(ncid, t700_td_in,t700_td_mean_x))) return retval; // Read the data. 
if ((retval = nc_close(ncid)))  return retval; // Close the file. 
//convert the demension
nn=0;
for (k=0;k<12;k++){
	for (i=0;i<n_lat;i++){
		for(j=0; j<n_lon;j++){
			phi500_mean[k][i][j]=phi500_mean_x[nn]; 
			W_mean[k][i][j]=W_mean_x[nn];
			t700_td_mean[k][i][j]=t700_td_mean_x[nn];
			//spei_mean[k][i][j]=spei_mean_x[nn];
			nn=nn+1;
		}
	}
}


/* calculate the anomaly*/
int year;
year=2011-1949+1; int n_year, count; double temp; 
double phi500_Mar2May[year][n_lat][n_lon], W_Sep2May[year][n_lat][n_lon], t700_td_Mar2May[year][n_lat][n_lon], spei_Jun2Aug[year][n_lat][n_lon];

//phi500's anomaly: 3 months averaged
//phi500_Mar2May(i,:,:)=dim_avg_n((f1->phi500(k:k+2,:,:)-f11->phi500_mean(2:4,:,:)),0)
n_year=0;
for (k=2;k<N-12;k+=12){
		for (i=0;i<n_lat;i++){
		for(j=0;j<n_lon;j++){
		
			for (n=0;n<3;n++){
				temp=0;//count=0;
				//if (phi500[k][i][j]!=1.0E30){ //skip missing value
				temp=temp+phi500[k+n][i][j]-phi500_mean[n][i][j];
				count=count+1;
				//}
			}
			phi500_Mar2May[n_year][i][j]=temp/count;
			
		}
	}
	
	n_year+=1;
}
//printf("phi500,%d\n",n_year);
//for (k=0;k<10;k++) printf("%e\n",phi500_Mar2May[k][0][0]);
//W(soil moisture)'s anomaly: 9 months accumulated
double W_mean_c[9][n_lat][n_lon]; 
	for (i=0;i<n_lat;i++){
		for(j=0; j<n_lon;j++){
		//Sep to Dec W_mean_c[0:3][i][j]=W_mean[8:11][i][j];
		W_mean_c[0][i][j]=W_mean[8][i][j]; 
		W_mean_c[1][i][j]=W_mean[9][i][j]; 
		W_mean_c[2][i][j]=W_mean[10][i][j]; 
		W_mean_c[3][i][j]=W_mean[11][i][j]; 
		//Jan to May W_mean_c[4:8][i][j]=W_mean[0:4][i][j]; 
		W_mean_c[4][i][j]=W_mean[0][i][j]; 
		W_mean_c[5][i][j]=W_mean[1][i][j]; 
		W_mean_c[6][i][j]=W_mean[2][i][j]; 
		W_mean_c[7][i][j]=W_mean[3][i][j]; 
		W_mean_c[8][i][j]=W_mean[4][i][j]; 
		}}
n_year=0;
for (k=8;k<N-8;k+=12){

	for (i=0;i<n_lat;i++){
		for(j=0; j<n_lon;j++){
		
			for (n=0;n<9;n++){
				temp=0;//count=0;
				//if (W[k][i][j]!=1.0E30){
				temp=temp+W[k+n][i][j]-W_mean_c[n][i][j];
				//count=count+1;
				//}
			}
			W_Sep2May[n_year][i][j]=temp;
			
		}
	}
	n_year+=1;
}
//printf("W,%d\n",n_year);
//T700_Td's anomaly: 3 months averaged
//T700_Td_Mar2May(i,:,:)=dim_avg_n((f1->T700_Td(k:k+2,:,:)-f11->T700_Td_mean(2:4,:,:)),0)
n_year=0;
for (k=2;k<N-12;k+=12){

	for (i=0;i<n_lat;i++){
		for(j=0;j<n_lon;j++){
		
			for (n=0;n<3;n++){
				temp=0;//count=0;
				//if (t700_td[k][i][j]!=1.0E30){ //skip missing value
				temp=temp+t700_td[k+n][i][j]-t700_td_mean[n][i][j];
				count=count+1;
				//}
			}
			t700_td_Mar2May[n_year][i][j]=temp/count;
			
		}
	}
	
	n_year+=1;
}
//printf("t700_td,%d\n",n_year);


//spei summer_JJA averaged index
//SPEI_Jun2Aug(i,:,:)=dim_avg_n(f->spei(k:k+2,:,:),0)
n_year=0;
for (k=5;k<N-12;k+=12){

	for (i=0;i<n_lat;i++){
		for(j=0;j<n_lon;j++){
		
			for (n=0;n<3;n++){
				temp=0,count=0;//
				if (fabs(spei[k][i][j]-1.0000000150474662E30)>1.0E-4){ //skip missing value 1.0000000150474662E30
				temp=temp+spei[k+n][i][j];
				count=count+1;
				}
			}
			if (count==0){
			spei_Jun2Aug[n_year][i][j]=1.0E30;//keep the missing value
			}
			else{
			spei_Jun2Aug[n_year][i][j]=temp/count;
			}
			
		}
	}
	
	n_year+=1;
}
//printf("spei,%d\n",n_year);

//********************landmask: mask the ocean, data from spei********************************
for (k=0;k<year;k++){
	for (i=0;i<n_lat;i++){
		for(j=0;j<n_lon;j++){
		//if (fabs(spei_Jun2Aug[k][i][j]-1.0000000150474662E30)<1.0E-3){
		if (fabs(spei_Jun2Aug[k][i][j]-1.0E30)<1.0E-6){
			phi500_Mar2May[k][i][j]=1.0E30;
			W_Sep2May[k][i][j]=1.0E30;
			t700_td_Mar2May[k][i][j]=1.0E30;
		}
		}
	}
}
double spei_Jun2Aug_x[year*n_lat*n_lon], phi500_Mar2May_x[year*n_lat*n_lon], W_Sep2May_x[year*n_lat*n_lon], t700_td_Mar2May_x[year*n_lat*n_lon];
nn=0;
for (k=0;k<year;k++){
	for (i=0;i<n_lat;i++){
		for(j=0; j<n_lon;j++){
		    spei_Jun2Aug_x[nn]=spei_Jun2Aug[k][i][j];
		    phi500_Mar2May_x[nn]=phi500_Mar2May[k][i][j];//			phi500[k][i][j]=phi500_x[nn]; 
			W_Sep2May_x[nn]=W_Sep2May[k][i][j];		//W[k][i][j]=W_x[nn];
			t700_td_Mar2May_x[nn]=t700_td_Mar2May[k][i][j];	   //t700_td[k][i][j]=t700_td_x[nn];
			nn=nn+1;
		}
	}
}
//***************************end landmask: mask the ocean, data from spei*************************************

//************************************write the anomaly to nc file********************************************
int time_dimid, lon_dimid, lat_dimid,  time_varid, lat_varid, lon_varid; 
int phi500_Mar2May_varid, W_Sep2May_varid, t700_td_Mar2May_varid, spei_Jun2Aug_varid;//int STNID_dimid, 
int NDIMS=3;
int dimids[NDIMS];
int yearN[year]; for (i=0;i<year;i++) yearN[i]=i+1949;

//write spei_Jun2Aug.nc file
float resolution;  resolution=2.5; //int nc_put_att_float     (int ncid, int varid, const char *name, nc_type xtype, size_t len, const float *fp);
double missing_var; missing_var=1.0E30;
if ((retval = nc_create("spei_Jun2Aug.nc", NC_CLOBBER|NC_NETCDF4, &ncid))) return retval;
/* Define the dimensions. */
if ((retval = nc_def_dim(ncid, "time", year, &time_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lat", n_lat, &lat_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lon", n_lon, &lon_dimid))) return retval;
/* Define the variables. */
dimids[0] =time_dimid;
dimids[1] =lat_dimid;
dimids[2] =lon_dimid;
if ((retval = nc_def_var(ncid, "time", NC_INT, 1, &dimids[0], &time_varid))) return retval;
if ((retval = nc_def_var(ncid, "lat", NC_DOUBLE, 1, &dimids[1], &lat_varid))) return retval;
if ((retval = nc_def_var(ncid, "lon", NC_DOUBLE, 1, &dimids[2], &lon_varid))) return retval;
if ((retval = nc_def_var(ncid, "spei_Jun2Aug", NC_DOUBLE, NDIMS, dimids, &spei_Jun2Aug_varid))) return retval;

if ((retval = nc_put_att_text(ncid, time_varid, "units", strlen("year"), "year"))) return retval;
if ((retval = nc_put_att_text(ncid, lat_varid, "units", strlen("degree_north"), "degree_north"))) return retval;
if ((retval = nc_put_att_float(ncid, lat_varid, "pointwidth", NC_FLOAT, 1, &resolution))) return retval;
if ((retval = nc_put_att_text(ncid, lon_varid, "units", strlen("degree_east"), "degree_east"))) return retval;   
if ((retval = nc_put_att_float(ncid, lon_varid, "pointwidth", NC_FLOAT, 1, &resolution))) return retval;  
if ((retval = nc_put_att_double(ncid, spei_Jun2Aug_varid, "_FillValue", NC_DOUBLE, 1, &missing_var))) return retval;  
if ((retval = nc_put_att_text(ncid, spei_Jun2Aug_varid, "long_name", strlen("Standardized Precipitation-Evapotranspiration Index, 6 months, JJA"), "Standardized Precipitation-Evapotranspiration Index, 6 months, JJA"))) return retval;  
/* End define mode. */
if ((retval = nc_enddef(ncid)))  return retval;
/* Write the data. */
if ((retval = nc_put_var_int(ncid, time_varid, yearN)))  return retval;
if ((retval = nc_put_var_double(ncid, lat_varid, lat)))  return retval;
if ((retval = nc_put_var_double(ncid, lon_varid, lon)))  return retval;
if ((retval = nc_put_var_double(ncid, spei_Jun2Aug_varid, spei_Jun2Aug_x)))  return retval;
/* Close the file. */
if ((retval = nc_close(ncid))) return retval;


//phi500_Mar2May.nc file
if ((retval = nc_create("phi500_Mar2May.nc", NC_CLOBBER|NC_NETCDF4, &ncid))) return retval;
/* Define the dimensions. */
if ((retval = nc_def_dim(ncid, "time", year, &time_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lat", n_lat, &lat_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lon", n_lon, &lon_dimid))) return retval;
/* Define the variables. */
dimids[0] =time_dimid;
dimids[1] =lat_dimid;
dimids[2] =lon_dimid;
if ((retval = nc_def_var(ncid, "time", NC_INT, 1, &dimids[0], &time_varid))) return retval;
if ((retval = nc_def_var(ncid, "lat", NC_DOUBLE, 1, &dimids[1], &lat_varid))) return retval;
if ((retval = nc_def_var(ncid, "lon", NC_DOUBLE, 1, &dimids[2], &lon_varid))) return retval;
if ((retval = nc_def_var(ncid, "phi500_Mar2May", NC_DOUBLE, NDIMS, dimids, &phi500_Mar2May_varid))) return retval;
/* End define mode. */
if ((retval = nc_enddef(ncid)))  return retval;
/* Write the data. */
if ((retval = nc_put_var_int(ncid, time_varid, yearN)))  return retval;
if ((retval = nc_put_var_double(ncid, lat_varid, lat)))  return retval;
if ((retval = nc_put_var_double(ncid, lon_varid, lon)))  return retval;
if ((retval = nc_put_var_double(ncid, phi500_Mar2May_varid, phi500_Mar2May_x)))  return retval;
/* Close the file. */
if ((retval = nc_close(ncid))) return retval;


//W_Sep2May.nc file
if ((retval = nc_create("W_Sep2May.nc", NC_CLOBBER|NC_NETCDF4, &ncid))) return retval;
/* Define the dimensions. */
if ((retval = nc_def_dim(ncid, "time", year, &time_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lat", n_lat, &lat_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lon", n_lon, &lon_dimid))) return retval;
/* Define the variables. */
dimids[0] =time_dimid;
dimids[1] =lat_dimid;
dimids[2] =lon_dimid;
if ((retval = nc_def_var(ncid, "time", NC_INT, 1, &dimids[0], &time_varid))) return retval;
if ((retval = nc_def_var(ncid, "lat", NC_DOUBLE, 1, &dimids[1], &lat_varid))) return retval;
if ((retval = nc_def_var(ncid, "lon", NC_DOUBLE, 1, &dimids[2], &lon_varid))) return retval;
if ((retval = nc_def_var(ncid, "W_Sep2May", NC_DOUBLE, NDIMS, dimids, &W_Sep2May_varid))) return retval;
/* End define mode. */
if ((retval = nc_enddef(ncid)))  return retval;
/* Write the data. */
if ((retval = nc_put_var_int(ncid, time_varid, yearN)))  return retval;
if ((retval = nc_put_var_double(ncid, lat_varid, lat)))  return retval;
if ((retval = nc_put_var_double(ncid, lon_varid, lon)))  return retval;
if ((retval = nc_put_var_double(ncid, W_Sep2May_varid, W_Sep2May_x)))  return retval;
/* Close the file. */
if ((retval = nc_close(ncid))) return retval;

//t700_td_Mar2May.nc file
if ((retval = nc_create("t700_td_Mar2May.nc", NC_CLOBBER|NC_NETCDF4, &ncid))) return retval;
/* Define the dimensions. */
if ((retval = nc_def_dim(ncid, "time", year, &time_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lat", n_lat, &lat_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lon", n_lon, &lon_dimid))) return retval;
/* Define the variables. */
dimids[0] =time_dimid;
dimids[1] =lat_dimid;
dimids[2] =lon_dimid;
if ((retval = nc_def_var(ncid, "time", NC_INT, 1, &dimids[0], &time_varid))) return retval;
if ((retval = nc_def_var(ncid, "lat", NC_DOUBLE, 1, &dimids[1], &lat_varid))) return retval;
if ((retval = nc_def_var(ncid, "lon", NC_DOUBLE, 1, &dimids[2], &lon_varid))) return retval;
if ((retval = nc_def_var(ncid, "t700_td_Mar2May", NC_DOUBLE, NDIMS, dimids, &t700_td_Mar2May_varid))) return retval;
/* End define mode. */
if ((retval = nc_enddef(ncid)))  return retval;
/* Write the data. */
if ((retval = nc_put_var_int(ncid, time_varid, yearN)))  return retval;
if ((retval = nc_put_var_double(ncid, lat_varid, lat)))  return retval;
if ((retval = nc_put_var_double(ncid, lon_varid, lon)))  return retval;
if ((retval = nc_put_var_double(ncid, t700_td_Mar2May_varid, t700_td_Mar2May_x)))  return retval;
/* Close the file. */
if ((retval = nc_close(ncid))) return retval;
//*******************************end write the anomaly to nc file******************************************

//**********do the multi-variable regression/multi-parameter fitting***************************************
//int gsl_multifit_linear (const gsl_matrix * X, const gsl_vector * y, gsl_vector * c, gsl_matrix * cov, double * chisq, gsl_multifit_linear_workspace * work)
//double x1[year][n_lat][n_lon], x2[year][n_lat][n_lon], x3[year][n_lat][n_lon], y0[year][n_lat][n_lon],chisq;
double x1, x2, x3, y0,chisq;
double B[4][n_lat][n_lon];
		gsl_matrix *X, *cov;
		gsl_vector *y, *c;
		X = gsl_matrix_alloc(year,4);
		y = gsl_vector_alloc(year);
		c = gsl_vector_alloc(4);
		cov = gsl_matrix_alloc(4,4);
#define C(i) gsl_vector_get(c,(i))
for (i=0;i<n_lat;i++){
	for(j=0; j<n_lon;j++){ 
		for (k=0;k<year;k++){
		x1=phi500_Mar2May[k][i][j];
		x2=W_Sep2May[k][i][j];
		x3=t700_td_Mar2May[k][i][j];
		y0=spei_Jun2Aug[k][i][j];
		gsl_matrix_set(X,k,0,1.0);
		gsl_matrix_set(X,k,1,x1);
		gsl_matrix_set(X,k,2,x2);
		gsl_matrix_set(X,k,3,x3);
		gsl_vector_set(y,k,y0);
		}
	gsl_multifit_linear_workspace *work=gsl_multifit_linear_alloc(year,4);
	gsl_multifit_linear(X,y,c,cov,&chisq,work);
	gsl_multifit_linear_free(work);
	B[0][i][j]=C(0);B[1][i][j]=C(1);B[2][i][j]=C(2);B[3][i][j]=C(3);
	}
}
//for (i=0;i<N;i++) printf("%e\n",B[i][0][0]);
//**********end the multi-variable regression/multi-parameter fitting***************************************


//********************************************write the coeficient to nc file******************************
//convert the demension
double B_x[4*n_lat*n_lon];
nn=0;
for (k=0;k<4;k++){
	for (i=0;i<n_lat;i++){
		for(j=0; j<n_lon;j++){
		    B_x[nn]=B[k][i][j];
		    nn=nn+1;
		}
	}
}
//B.nc file
int B_varid;
if ((retval = nc_create("B.nc", NC_CLOBBER|NC_NETCDF4, &ncid))) return retval;
/* Define the dimensions. */
if ((retval = nc_def_dim(ncid, "lat", n_lat, &lat_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lon", n_lon, &lon_dimid))) return retval;
/* Define the variables. */
NDIMS=2;//dimids[0] =time_dimid;
dimids[0] =lat_dimid;
dimids[1] =lon_dimid;
if ((retval = nc_def_var(ncid, "lat", NC_DOUBLE, 1, &dimids[0], &lat_varid))) return retval;
if ((retval = nc_def_var(ncid, "lon", NC_DOUBLE, 1, &dimids[1], &lon_varid))) return retval;
if ((retval = nc_def_var(ncid, "B", NC_DOUBLE, NDIMS, dimids, &B_varid))) return retval;
/* End define mode. */
if ((retval = nc_enddef(ncid)))  return retval;
/* Write the data. */
if ((retval = nc_put_var_double(ncid, lat_varid, lat)))  return retval;
if ((retval = nc_put_var_double(ncid, lon_varid, lon)))  return retval;
if ((retval = nc_put_var_double(ncid, B_varid, B_x)))  return retval;
/* Close the file. */
if ((retval = nc_close(ncid))) return retval;
//******************************************end write the coeficient to nc file*****************************

//********reconstruct and normalize*******************
double IFDW[year][n_lat][n_lon],IFDW_raw[year][n_lat][n_lon];
for (k=0;k<year;k++){
	for (i=0;i<n_lat;i++){
		for(j=0; j<n_lon;j++){
		IFDW_raw[k][i][j]=B[0][i][j]+phi500_Mar2May[k][i][j]*B[1][i][j]+W_Sep2May[k][i][j]*B[2][i][j]+t700_td_Mar2May[k][i][j]*B[3][i][j];
		}
	}
}
//for (i=0;i<73;i++) printf("%e\n",IFDW_raw[0][i][0]);
//normalize/standardize
double std[n_lat][n_lon], data[year],IFDW_raw_mean[n_lat][n_lon];
//the mean and std
for (i=0;i<n_lat;i++){
	for(j=0; j<n_lon;j++){
		for (k=0;k<year;k++){
		data[k]=IFDW_raw[k][i][j];
		}
		IFDW_raw_mean[i][j] = gsl_stats_mean(data, 1, year);
		std[i][j]=gsl_stats_sd (data, 1, year);
	}
}
for (i=0;i<n_lat;i++){
	for(j=0; j<n_lon;j++){
		for (k=0;k<year;k++){
		IFDW[k][i][j]=(IFDW_raw[k][i][j]-IFDW_raw_mean[i][j])/std[i][j];
		}
	}
}
//landmask
for (k=0;k<year;k++){
	for (i=0;i<n_lat;i++){
		for(j=0;j<n_lon;j++){
		//if (fabs(spei_Jun2Aug[k][i][j]-1.0000000150474662E30)<1.0E-3){
		if (fabs(spei_Jun2Aug[k][i][j]-1.0E30)<1.0E-6){
			IFDW[k][i][j]=1.0E30;
		}
		}
	}
}

//*********end reconstruct and normalize**************

//**********the output********************
//convert the demension

double IFDW_x[year*n_lat*n_lon];
nn=0;
for (k=0;k<year;k++){
	for (i=0;i<n_lat;i++){
		for(j=0; j<n_lon;j++){
		    IFDW_x[nn]=IFDW[k][i][j];
			nn=nn+1;
		}
	}
}
int IFDW_varid;NDIMS=3; 
//write IFDW.nc file
if ((retval = nc_create("IFDW.nc", NC_CLOBBER|NC_NETCDF4, &ncid))) return retval;
/* Define the dimensions. */
if ((retval = nc_def_dim(ncid, "time", year, &time_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lat", n_lat, &lat_dimid))) return retval;
if ((retval = nc_def_dim(ncid, "lon", n_lon, &lon_dimid))) return retval;
/* Define the variables. */
dimids[0] =time_dimid;
dimids[1] =lat_dimid;
dimids[2] =lon_dimid;
if ((retval = nc_def_var(ncid, "time", NC_INT, 1, &dimids[0], &time_varid))) return retval;
if ((retval = nc_def_var(ncid, "lat", NC_DOUBLE, 1, &dimids[1], &lat_varid))) return retval;
if ((retval = nc_def_var(ncid, "lon", NC_DOUBLE, 1, &dimids[2], &lon_varid))) return retval;
if ((retval = nc_def_var(ncid, "IFDW", NC_DOUBLE, NDIMS, dimids, &IFDW_varid))) return retval;

// Define units attributes for vars. 

if ((retval = nc_put_att_text(ncid, time_varid, "units", strlen("year"), "year"))) return retval;
if ((retval = nc_put_att_text(ncid, lat_varid, "units", strlen("degree_north"), "degree_north"))) return retval;
if ((retval = nc_put_att_float(ncid, lat_varid, "pointwidth", NC_FLOAT, 1, &resolution))) return retval;
if ((retval = nc_put_att_text(ncid, lon_varid, "units", strlen("degree_east"), "degree_east"))) return retval;   
if ((retval = nc_put_att_float(ncid, lon_varid, "pointwidth", NC_FLOAT, 1, &resolution))) return retval;  
if ((retval = nc_put_att_double(ncid, IFDW_varid, "_FillValue", NC_DOUBLE, 1, &missing_var))) return retval;  
if ((retval = nc_put_att_text(ncid, IFDW_varid, "long_name", strlen("Indicator of Flash Drought Warming"), "Indicator of Flash Drought Warming"))) return retval;  
/* End define mode. */
if ((retval = nc_enddef(ncid)))  return retval;
/* Write the data. */
if ((retval = nc_put_var_int(ncid, time_varid, yearN)))  return retval;
if ((retval = nc_put_var_double(ncid, lat_varid, lat)))  return retval;
if ((retval = nc_put_var_double(ncid, lon_varid, lon)))  return retval;
if ((retval = nc_put_var_double(ncid, IFDW_varid, IFDW_x)))  return retval;
/* Close the file. */
if ((retval = nc_close(ncid))) return retval;
//********end the output**************



return 0;
}
