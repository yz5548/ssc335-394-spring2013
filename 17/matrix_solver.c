#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "mmio.h"
#include "read_mm.h"
#include <petscksp.h>
//#include "petsc.h"
#undef __FUNCT__
#define __FUNCT__ "main"
//#define __FUNCT__ "matrix_solver"


static char help[] = "Solves a tridiagonal linear system with KSP.\n\n";

int matrix_solver(double* V, double* X, double* B, int n)
{
  double l2_norm;
  int i;
  l2_norm=0.0;
  //Mat* A, 
  Vec            x, b;      /* approx solution, RHS, exact solution */
  Mat            A;            /* linear system matrix */
  KSP            ksp;         /* linear solver context */
  PetscScalar    v[n*n],x_arr[n],b_arr[n];
  PetscInt       rows[n],cols[n],index[n];
  //int            its;                      /* problem dimension, number of iterations */
  //PC             pc;           /* preconditioner context */
  //PetscReal      norm,tol=1.e-14;  /* norm of solution error */
  PetscErrorCode ierr;
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
         Compute the matrix and right-hand-side vector that define
         the linear system, Ax = b.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = MatCreate(PETSC_COMM_WORLD,&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,n,n);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  ierr = MatSetUp(A);CHKERRQ(ierr);
  
  ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,n);CHKERRQ(ierr);
  ierr = VecSetFromOptions(x);CHKERRQ(ierr);
  ierr = VecSetUp(x);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&b);CHKERRQ(ierr);
  
  // Assemble vector
  for(i=0;i<n;i++) {
  index[i]=i;
  b_arr[i]=*(B+i);
  x_arr[i]=*(X+i);
  }
  
  ierr = VecSetValues(x, n, index, x_arr, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(x);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(x);CHKERRQ(ierr);
  
  ierr = VecSetValues(b, n, index, b_arr, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(b);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(b);CHKERRQ(ierr);
  /* 
     Create matrix.  When using MatCreate(), the matrix format can
     be specified at runtime.

     Performance tuning note:  For problems of substantial size,
     preallocation of matrix memory is crucial for attaining good 
     performance. See the matrix chapter of the users manual for details.
  */


  // Assemble matrix
  for(i=0;i<n*n;i++) v[i]=*(V+i); //read the value
  for(i=0;i<n;i++) {
  cols[i]=i;
  rows[i]=i;
  }
  ierr=MatSetValues(A, n, rows, n, cols, v, INSERT_VALUES);
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD, &ksp);;CHKERRQ(ierr); 
  ierr = KSPSetOperators(ksp, A, A, DIFFERENT_NONZERO_PATTERN);;CHKERRQ(ierr); 
  ierr = KSPSetFromOptions(ksp);;CHKERRQ(ierr); 
  ierr = KSPSolve(ksp, b, x);;CHKERRQ(ierr); 
     
  //ierr = MatView(A,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); //PETSC_VIEWER_STDOUT_WORLD 
  //ierr = VecView(b,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  //ierr = VecView(x,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  //ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
 
  //ierr = VecGetArray(x,&x_arr);CHKERRQ(ierr); //VecRestoreArray
  //printf("%f\n",x_arr);
  //ierr = VecRestoreArray(x,&x_arr);CHKERRQ(ierr);
 
  ierr = VecGetValues(x,n,index,X); CHKERRQ(ierr);
  //for(i=0;i<n;i++) *(X+i)=x_arr[i];  // b_arr[i]=*(B+i);  x_arr[i]=*(X+i);
  for (i = 0; i < n; ++i) l2_norm += X[i]*X[i];
  printf("  L2 norm of solver: %f\n", sqrt(l2_norm));
 
  ierr = VecDestroy(&x);CHKERRQ(ierr); 
  ierr = VecDestroy(&b);CHKERRQ(ierr); 
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);;CHKERRQ(ierr); 
  //ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  //ierr = PetscFinalize();CHKERRQ(ierr);
  //free(V);free(X);free(B);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}

int test_matrix_solver(){
  double A[9] = {1, 2, 3, 2, -4, 6, 3, -9, -3};
  double x[3]; 
  double b[3] = {5, 18, 6};
  //double error;PetscErrorCode ierr;
  int i;//p,i,j,k,M;  M=3;
  printf("This is a test:\n");
  matrix_solver(A, x, b, 3);
  printf("The computed solution of this test equation is (format: x1 x2 x3):\n");
  for(i=0;i<3;i++) printf("%4.2f ",x[i]); printf("\n");
  return 0;
}

int main(int argc, char** argv)
{
  int ret_code, M, N, i;
  double *A=NULL, *x, *b;
  FILE *f;
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc,&argv,(char *)0,help);CHKERRQ(ierr);
    if (argc < 2) {
    fprintf(stderr, "Usage: %s [martix-market-filename | test]\n", argv[0]);
    return 1;
  }

  if (!strcmp(argv[1], "test")) {
    return test_matrix_solver();
  }

  if ((f = fopen(argv[1], "r")) == NULL) {
    printf("Could not open file.\n");
    return 1;
  }

  ret_code = read_mm(f, &A, &M, &N);
  if (ret_code != 0){
    printf("Error opening Matrix Market file\n");
    return 1;
  } else {
    x = (double*) malloc(M*sizeof(double));
    b = (double*) malloc(M*sizeof(double));
    for (i=0; i<M; ++i) b[i] = 1;
    ret_code = matrix_solver(A, x, b, M);
    free(x);
    free(b);
    return ret_code;
  }
  //ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
